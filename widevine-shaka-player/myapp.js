const manifestUri = 'https://d3nzo6itypaz07.cloudfront.net/8132cfc8-34fe-457d-a8fb-185179bdbc82/master.mpd'

function initApp() {
  // Install built-in polyfills to patch browser incompatibilities.
  shaka.polyfill.installAll();

  // Check to see if the browser supports the basic APIs Shaka needs.
  if (shaka.Player.isBrowserSupported()) {
    // Everything looks good!
    initPlayer();
  } else {
    // This browser does not have the minimum set of APIs we need.
    console.error('Browser not supported!');
  }
}

async function initPlayer() {
  // Create a Player instance.
  const video = document.getElementById('video');
  const player = new shaka.Player(video);

  // Attach player to the window to make it easy to access in the JS console.
  window.player = player;
  player.resetConfiguration()
  // Listen for error events.
  player.addEventListener('error', onErrorEvent);

//drm configuration
  player.configure({
    drm: {
      servers: {
        'com.widevine.alpha': 'https://license.vdocipher.com/auth/wv/eyJjb250ZW50QXV0aCI6ImV5SmpiMjUwWlc1MFNXUWlPaUpqWkRaa01qSTNZelV4WTJWbVlqQTFNVEk0TWpFMU56VXlZall5WkRoa1pTSXNJbVY0Y0dseVpYTWlPakUyTnpZMk1qTTFOREY5Iiwic2lnbmF0dXJlIjoiVHB0ajRBOWx0U3JoaXlBMDoyMDIzMDIxN1QwNzQ1NDE2MTVaOnk0Tjg3S1IzRnN1d2pQYTZMVWNiNE84X0RfV1ltdXNlY1FMdFBLRkc1clk9In0='
      }
    }
  });
  player.configure('drm.clearKeys', undefined)

  //clear key configuration
//   player.configure({
//     drm: {
//         clearKeys: {
//         '4a17f19e8b7c74cd5971a36eac37e579': '8ed90e6a54a505c1e9684dc4721e8b2d'
//       }
//     }
//   });

  // Try to load a manifest.
  // This is an asynchronous process.
  try {
    await player.load(manifestUri);
    // This runs if the asynchronous load is successful.
    console.log('The video has now been loaded!');
  } catch (e) {
    // onError is executed if the asynchronous load fails.
    onError(e);
  }
}

function onErrorEvent(event) {
  // Extract the shaka.util.Error object from the event.
  onError(event.detail);
}

function onError(error) {
  // Log the error.
  console.log(error);
  console.error('Error code', error.code, 'object', error);
}

document.addEventListener('DOMContentLoaded', initApp);