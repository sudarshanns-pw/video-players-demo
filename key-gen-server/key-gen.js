var express = require('express');
var crypto = require('crypto')
const request = require("request");
var app = express();

const APIKEY = "Tptj4A9ltSrhiyA0OVvu8hZu1KQEo1kLKMCn8pEcpz9e1LBfQylE8qj7Cm62Kb0B";

const hmac = (key, input) => {
    const hm = crypto.createHmac("sha256", key);
    hm.update(input);
    return hm.digest();
};

const urlSafeB64 = (input) => {
    return input.toString("base64").replace(/\//g, "_").replace(/\+/g, "-");
};

app.get('/', function(req, res) {
    var id = req.query.id;
    var type = req.query.type;
    console.log(id)
    const CONTENT_ID = id;
    const DRM_TYPE = type ? type : "wv"; // fps or wv
    const timestamp = Math.floor(new Date().getTime() / 1000);
    const contentAuthObj = {
        contentId: CONTENT_ID,
        expires: timestamp + 172800,
    };
    const signingDate = new Date().toISOString().replace(/[-.:]/g, "");
    const contentAuthStr = urlSafeB64(Buffer.from(JSON.stringify(contentAuthObj)));
    const signedDate = hmac(APIKEY, signingDate);
    const hash = urlSafeB64(hmac(signedDate, contentAuthStr));
    const keyId = APIKEY.substr(0, 16);
    const signature = `${keyId}:${signingDate}:${hash}`;
    const LICENSE_URL =
        `https://license.vdocipher.com/auth/${DRM_TYPE}/` +
        urlSafeB64(
            Buffer.from(
                JSON.stringify({
                    contentAuth: `${contentAuthStr}`,
                    signature: `${signature}`,
                })
            )
        );

    res.end(LICENSE_URL)
});

app.get('/fps-keys', function (req, res) {
    const APIKEY = 'Tptj4A9ltSrhiyA0OVvu8hZu1KQEo1kLKMCn8pEcpz9e1LBfQylE8qj7Cm62Kb0B';

    // use random bytes generation here
    // here this reads from argument which works well for examples
    // this should be saved in the database corresponding to content
    const contentIdHexa = crypto.randomBytes(16).toString("hex")
    const contentId = Buffer.from(contentIdHexa, 'hex').toString("base64");

    console.log("content ids in hexa and base64", contentIdHexa, contentId)

    /**
     * @param {string|Buffer} key secret in utf8
     * @param {string} input utf8 input
     * @return {Buffer}
     */
    const hmac = (key, input) => {
        const hm = crypto.createHmac("sha256", key);
        hm.update(input);
        return hm.digest();
    };

    const keyRequestObj = { contentId };
    const signingDate = new Date().toISOString().replace(/[-.:]/g, "");
    const keyRequest = JSON.stringify(keyRequestObj);
    const hash = hmac(hmac(APIKEY, signingDate), keyRequest).toString("base64");
    const keyId = APIKEY.substr(0, 16);
    const signature = `${keyId}:${signingDate}:${hash}`;

    request({
        url: `https://license.vdocipher.com/content-keys/fps/pwlive`,
        method: "POST",
        json: true,
        body: { signature, keyRequest },
    }, (e, response, responseBody) => {
        if (e) throw e;
        console.log(new Date())
        if (response.statusCode === 200) {
            console.log(responseBody);
        } else {
            console.log('HTTP Status: ', response.statusCode);
            console.log('Response Body: ', responseBody);
        }
    })
    res.end();
});

app.listen(8082, () => console.log("App started on 8082"));